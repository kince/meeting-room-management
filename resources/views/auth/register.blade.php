<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link href="{{asset('vendor/select2/select2.min.css')}}" rel="stylesheet" />
    <title>Register</title>

    @vite('resources/css/app.css')
    @vite('resources/js/app.js')
    <script src="{{asset('vendor/jquery/jquery-3.7.1.min.js')}}"></script>
    <script src="{{asset('vendor/select2/select2.min.js')}}"></script>
    <style>
        .select2.select2-container .select2-selection {
            border: 1px solid #ccc;
            -webkit-border-radius: 3px;
            -moz-border-radius: 3px;
            border-radius: 3px;
            height: 35px;
            margin-bottom: 15px;
            outline: none !important;
            transition: all .15s ease-in-out;
        }

        .select2.select2-container .select2-selection .select2-selection__rendered {
            color: #333;
            line-height: 32px;
            padding-right: 33px;
        }

        .select2.select2-container .select2-selection .select2-selection__arrow {
            background: #f8f8f8;
            border-left: 1px solid #ccc;
            -webkit-border-radius: 0 3px 3px 0;
            -moz-border-radius: 0 3px 3px 0;
            border-radius: 0 3px 3px 0;
            height: 32px;
            width: 33px;
        }

        .select2.select2-container .select2-selection--multiple .select2-search--inline .select2-search__field {
            margin-top: 0;
            height: 34px;
        }

        .select2-container .select2-dropdown {
            /* border: none; */
            margin-top: -8px;
        }

        .select2-container .select2-dropdown .select2-search input {
            outline: none !important;
            border: 1px solid #34495e !important;
            border-bottom: none !important;
            padding: 4px 6px !important;
        }

        .select2-container .select2-dropdown .select2-results ul {
            background: #fff;
            border: 1px solid #34495e;
        }

        .select2-container .select2-dropdown .select2-results ul .select2-results__option--highlighted[aria-selected] {
            background-color: #3498db;
        }
    </style>
</head>

<body class="bg-gray-100 flex h-full items-center py-16">
    <main class="w-full max-w-2xl mx-auto p-6">
        <div class="mt-7 bg-white border border-gray-200 rounded-xl shadow-sm">
            <div class="p-4 sm:p-7">
                <div class="text-center">
                    <h1 class="block text-2xl font-bold text-gray-800">Register</h1>
                </div>

                <div class="mt-5">
                    <!-- Form -->
                    <form method="POST" action="{{route('register')}}">
                        @csrf
                        <div class="grid grid-cols-2 gap-4">

                            <div class="col-span-2">
                                <label for="type" class="block text-sm mb-2">Account type</label>
                                <div class="relative">
                                    <select class="user-type-select py-3 px-4 block w-full" name="type">
                                        <option value="client">Simple user</option>
                                        <option value="announcer">Announcer</option>
                                    </select>
                                </div>
                            </div>

                            <!-- Form Group -->
                            <div class="col-span-2">
                                <label for="name" class="block text-sm mb-2">Full name</label>
                                <div class="relative">
                                    <input type="text" id="name" name="name"
                                        class="py-3 px-4 block w-full border-gray-200 rounded-lg text-sm focus:border-blue-500 focus:ring-blue-500 disabled:opacity-50 disabled:pointer-events-none">
                                </div>
                            </div>
                            <!-- End Form Group -->

                            <!-- Form Group -->
                            <div>
                                <label for="phone" class="block text-sm mb-2">Phone number</label>
                                <div class="relative">
                                    <input type="text" id="phone" name="phone"
                                        class="py-3 px-4 block w-full border-gray-200 rounded-lg text-sm focus:border-blue-500 focus:ring-blue-500 disabled:opacity-50 disabled:pointer-events-none">
                                </div>
                            </div>

                            <div>
                                <label for="email" class="block text-sm mb-2">Email address</label>
                                <div class="relative">
                                    <input type="email" id="email" name="email"
                                        class="py-3 px-4 block w-full border-gray-200 rounded-lg text-sm focus:border-blue-500 focus:ring-blue-500 disabled:opacity-50 disabled:pointer-events-none"
                                        required aria-describedby="email-error">
                                </div>
                            </div>

                            <div>
                                <label for="address" class="block text-sm mb-2">Addresse</label>
                                <div class="relative">
                                    <input type="text" id="address" name="address"
                                        class="py-3 px-4 block w-full border-gray-200 rounded-lg text-sm focus:border-blue-500 focus:ring-blue-500 disabled:opacity-50 disabled:pointer-events-none">
                                </div>
                            </div>
                            <!-- End Form Group -->


                            <!-- Form Group -->
                            <div>
                                <div class="flex justify-between items-center">
                                    <label for="password" class="block text-sm mb-2">Password</label>
                                    <a class="text-sm text-blue-600 decoration-2 hover:underline font-medium"
                                        href="../examples/html/recover-account.html">Forgot password?</a>
                                </div>
                                <div class="relative">
                                    <input type="password" id="password" name="password"
                                        class="py-3 px-4 block w-full border-gray-200 rounded-lg text-sm focus:border-blue-500 focus:ring-blue-500 disabled:opacity-50 disabled:pointer-events-none"
                                        required aria-describedby="password-error">

                                </div>
                            </div>
                            <!-- End Form Group -->

                            <!-- Checkbox -->
                            <div class="col-span-2 flex items-center py-2.5">
                                <button type="submit"
                                    class="col-span-2 w-full py-3 px-4 inline-flex justify-center items-center gap-x-2 text-sm font-semibold rounded-lg border border-transparent bg-blue-600 text-white hover:bg-blue-700 disabled:opacity-50 disabled:pointer-events-none">
                                    Register
                                </button>
                            </div>
                            <!-- End Checkbox -->

                        </div>
                    </form>
                    <!-- End Form -->
                </div>
            </div>
        </div>
    </main>
</body>
<script>
    $(document).ready(function() {
        $('.user-type-select').select2({
            placeholder: 'Select type',
            allowClear: true,
        });
    });
</script>
@include('sweetalert::alert')

</html>
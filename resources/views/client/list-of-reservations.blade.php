@extends("client.layout.base")

@section("content")
<main class="app-wrapper">
    <div class="flex flex-col justify-between min-h-screen">
        <div class="content-wrapper transition-all duration-150" id="content_wrapper">
            <div class="transition-all duration-150 container-fluid" id="page_layout">
                <div class="m-5">
                    <div class="p-8">
                        <table class="min-w-full divide-y divide-gray-200">
                            <thead>
                                <tr>
                                    <th
                                        class="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
                                        Meeting Rooms
                                    </th>
                                    <th
                                        class="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
                                        Reservation Date
                                    </th>
                                    <th
                                        class="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
                                        Start at
                                    </th>
                                    <th
                                        class="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
                                        End at
                                    </th>
                                    <th
                                        class="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
                                        Status
                                    </th>
                                    <th
                                        class="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
                                        Action</th>
                                </tr>
                            </thead>
                            <tbody class="bg-white divide-y divide-gray-200">
                                @foreach ($reservations as $reservation)

                                <tr>
                                    <td class="px-6 py-4 whitespace-nowrap">
                                        {{$reservation->meetingRoom->name}}
                                    </td>
                                    <td class="px-6 py-4 whitespace-nowrap">
                                        {{$reservation->created_at}}
                                    </td>
                                    <td class="px-6 py-4 whitespace-nowrap">
                                        {{$reservation->start_date}}
                                    </td>
                                    <td class="px-6 py-4 whitespace-nowrap">
                                        {{$reservation->end_date}}
                                    </td>
                                    <td class="px-6 py-4 whitespace-nowrap">
                                        <span
                                            class="px-2 inline-flex text-xs leading-5 font-semibold rounded-full bg-red-100 text-red-800">
                                            {{$reservation->status}}
                                        </span>
                                    </td>
                                    <td class="flex px-6 py-4 whitespace-nowrap">
                                        @if ($reservation->status != "canceled" && $reservation->status != "finished")
                                        <a href="{{route('client.reservations.cancel',$reservation->id)}}"
                                            class="px-4 py-2 font-medium text-white bg-blue-600 rounded-md hover:bg-blue-500 focus:outline-none focus:shadow-outline-blue active:bg-blue-600 transition duration-150 ease-in-out">
                                            Cancel
                                        </a>
                                        @endif
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>
@endsection
@extends("client.layout.base")

@section("content")
<main class="app-wrapper">
    <div class="flex flex-col justify-between min-h-screen">
        <div class="content-wrapper transition-all duration-150" id="content_wrapper">
            <div class="transition-all duration-150 container-fluid" id="page_layout">
                <div class="m-5">
                    <div class="p-8">
                        <div class="grid xl:grid-cols-4 lg:grid-cols-3 md:grid-cols-2 grid-cols-1 gap-5">

                            @foreach ($meeting_rooms as $meeting_room)
                            @if ($meeting_room->is_available())
                            <div class="card border rounded-md shadow-xl">
                                <div>
                                    <div class=" h-[248px] w-full mb-6 ">
                                        <img src="{{ asset('storage/mr-images/' . $meeting_room->medias->first()->file_path) }}"
                                            alt="" class="w-full h-full object-cover">
                                    </div>
                                    <div class="p-5 pb-6">
                                        <div class="flex justify-between mb-4">
                                            <div>
                                                <h5 class="card-title text-slate-900 text-xl font-bold">
                                                    <a
                                                        href="{{route('client.show_mr',$meeting_room->id)}}">{{$meeting_room->name}}</a>
                                                </h5>
                                            </div>
                                            <a href="#"
                                                class="inline-flex leading-5 text-slate-500 text-sm font-normal">
                                                <span>
                                                    <iconify-icon icon="heroicons-outline:calendar"
                                                        class="text-slate-400 ltr:mr-2 rtl:ml-2 text-lg">
                                                    </iconify-icon>
                                                </span>
                                                <span>
                                                    {{$meeting_room->created_at->format("d/m/Y")}}
                                                </span>
                                            </a>
                                        </div>
                                        <div class="card-text text-slate-900 mt-4">
                                            <p>
                                                {{Str::limit($meeting_room->description?$meeting_room->description:
                                                "Aucune description", 60, "...")}}
                                            </p>
                                            <div class="mt-4 space-x-4 rtl:space-x-reverse">
                                                <a href="{{route('client.show_mr',$meeting_room->id)}}" class="btn-a">
                                                    Learn more
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                    {{-- <div class="grid grid-cols-2 w-full">
                                        <button class="p-2.5 font-medium border">Book</button>
                                        <button class="p-2.5 font-medium border">View more</button>
                                    </div> --}}
                                </div>
                            </div>
                            @endif
                            @endforeach

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>
@endsection
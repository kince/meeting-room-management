@extends("client.layout.base")

@section("content")
<main class="app-wrapper">
    <div class="flex flex-col justify-between min-h-screen">
        <div class="content-wrapper transition-all duration-150" id="content_wrapper">
            <div class="transition-all duration-150 container-fluid" id="page_layout">
                <div class="m-5">
                    <div class="p-8">
                        <div class="max-w-screen-lg mx-auto p-5 sm:p-10 md:p-16">

                            <div class="mb-10 rounded overflow-hidden flex flex-col mx-auto">
                                <h4
                                    class="text-xl sm:text-4xl font-semibold hover:text-indigo-600 transition duration-500 ease-in-out inline-block mb-4">
                                    {{$meeting_room->name}}
                                </h4>

                                <div class="grid grid-cols-3 gap-4">
                                    @foreach ($meeting_room->medias as $key => $image)
                                    <div class="">
                                        <div class="team-single-box">
                                            <div class="team-thumb">
                                                <img src="{{ asset('storage/mr-images/' . $image->file_path) }}"
                                                    class="w-100" style="height: 230px" alt="">
                                            </div>
                                        </div>
                                    </div>
                                    @endforeach
                                </div>
                                <p class="text-gray-700 py-5 text-base leading-8">
                                    {{$meeting_room->description}}
                                </p>
                                <div class="py-5 text-sm font-regular text-gray-900 flex">
                                    <span class="mr-3 flex flex-row items-center">
                                        <svg class="text-indigo-600" fill="currentColor" height="13px" width="13px"
                                            version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg"
                                            xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                                            viewBox="0 0 512 512" style="enable-background:new 0 0 512 512;"
                                            xml:space="preserve">
                                            <g>
                                                <g>
                                                    <path
                                                        d="M256,0C114.837,0,0,114.837,0,256s114.837,256,256,256s256-114.837,256-256S397.163,0,256,0z M277.333,256
                                                        c0,11.797-9.536,21.333-21.333,21.333h-85.333c-11.797,0-21.333-9.536-21.333-21.333s9.536-21.333,21.333-21.333h64v-128
                                                        c0-11.797,9.536-21.333,21.333-21.333s21.333,9.536,21.333,21.333V256z">
                                                    </path>
                                                </g>
                                            </g>
                                        </svg>
                                        <span class="ml-1">{{$meeting_room->created_at->format("d/m/Y")}}</span>
                                    </span>

                                    <a class="flex flex-row items-center hover:text-indigo-600">
                                        <svg class="text-indigo-600" fill="currentColor" height="16px"
                                            aria-hidden="true" role="img" focusable="false" viewBox="0 0 24 24"
                                            xmlns="http://www.w3.org/2000/svg">
                                            <path fill="currentColor"
                                                d="M12 12c2.21 0 4-1.79 4-4s-1.79-4-4-4-4 1.79-4 4 1.79 4 4 4zm0 2c-2.67 0-8 1.34-8 4v2h16v-2c0-2.66-5.33-4-8-4z">
                                            </path>
                                            <path d="M0 0h24v24H0z" fill="none"></path>
                                        </svg>
                                        <span class="ml-1">{{$meeting_room->owner->name}}</span>
                                    </a>
                                </div>
                                <hr>

                                <div class="">
                                    <form action="{{route('client.book')}}" method="post">
                                        <div class="grid grid-cols-4 gap-4">
                                            @csrf
                                            <input type="hidden" name="meeting_room_id" value="{{$meeting_room->id}}">
                                            <div class="">
                                                <label for="start_date"
                                                    class="text-sm font-medium text-gray-900 block mb-2">
                                                    Date de début
                                                </label>
                                                <input type="date" name="start_date" id="start_date"
                                                    class="shadow-sm bg-gray-50 border border-gray-300 text-gray-900 sm:text-sm rounded-lg focus:ring-cyan-600 focus:border-cyan-600 block w-full p-2.5">
                                            </div>
                                            <div class="">
                                                <label for="end_date"
                                                    class="text-sm font-medium text-gray-900 block mb-2">
                                                    Date de fin
                                                </label>
                                                <input type="date" name="end_date" id="end_date"
                                                    class="shadow-sm bg-gray-50 border border-gray-300 text-gray-900 sm:text-sm rounded-lg focus:ring-cyan-600 focus:border-cyan-600 block w-full p-2.5">
                                            </div>
                                            <div class="">
                                                <button type="submit"
                                                    class="shadow-sm mt-7 bg-indigo-700 text-white border border-gray-300 font-semibold sm:text-sm rounded-lg focus:ring-cyan-600 focus:border-cyan-600 block w-full p-2.5">
                                                    Book
                                                </button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>
<script>
    $("#start_date").flatpickr({
        enableTime: true,
        dateFormat: "Y-m-d H:i",
        minDate: "today",
        // mode: "range",
    });
    $("#end_date").flatpickr({
        enableTime: true,
        dateFormat: "Y-m-d H:i",
        minDate: "today",
        // mode: "range",
    });
</script>
@endsection
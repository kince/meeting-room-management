@extends("dashboard.layout.base")

@section("content")
<main class="container mx-w-6xl mx-auto py-4">
    <div class="flex-auto block py-8 pt-6 px-9">
        <div class="overflow-x-auto">
            <table class="w-full my-0 align-middle text-dark border-neutral-200">
                <thead class="align-bottom">
                    <tr class="font-semibold text-[0.95rem] text-secondary-dark">
                        <th class="pb-3 text-start min-w-[175px]">Label</th>
                        <th class="pb-3 text-end min-w-[100px]">Address</th>
                        <th class="pb-3 text-end min-w-[100px]">Contact</th>
                        <th class="pb-3 pr-12 text-end min-w-[175px]">Capacity</th>
                        <th class="pb-3 pr-12 text-end min-w-[100px]">Price</th>
                        <th class="pb-3 text-end min-w-[50px]">Created at</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($meeting_rooms as $meeting_room)
                    <tr class="border-b border-dashed last:border-b-0">
                        <td class="p-3 pl-0">
                            <div class="flex items-center">
                                <div class="relative inline-block shrink-0 rounded-2xl me-3">
                                    <img src="{{ asset('storage/mr-images/' . $meeting_room->medias->first()->file_path) }}"
                                        class="w-[50px] h-[50px] inline-block shrink-0 rounded-2xl" alt="">
                                </div>
                                <div class="flex flex-col justify-start">
                                    <h4
                                        class="mb-1 font-semibold transition-colors duration-200 ease-in-out text-lg/normal text-secondary-inverse hover:text-primary">
                                        {{$meeting_room->name}}
                                    </h4>
                                </div>
                            </div>
                        </td>
                        <td class="p-3 pr-0 text-end">
                            <span class="font-semibold text-light-inverse text-md/normal">
                                {{$meeting_room->address."/".$meeting_room->address2}}
                            </span>
                        </td>
                        <td class="p-3 pr-0 text-end">
                            {{$meeting_room->contact1." / ".$meeting_room->contact2}}
                        </td>
                        <td class="p-3 pr-12 text-end">
                            <span
                                class="text-center align-baseline inline-flex px-4 py-3 mr-auto items-center font-semibold text-[.95rem] leading-none text-primary bg-primary-light rounded-lg">
                                {{$meeting_room->capacity}}
                            </span>
                        </td>
                        <td class="pr-0 text-start">
                            <span class="font-semibold text-light-inverse text-md/normal">
                                {{$meeting_room->rental_cost}}
                            </span>
                        </td>
                        <td class="p-3 pr-0 text-end">
                            <span>
                                {{$meeting_room->created_at}}
                            </span>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</main>
@endsection
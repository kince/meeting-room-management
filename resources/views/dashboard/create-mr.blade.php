@extends("dashboard.layout.base")

@section("content")
<main class="container mx-w-6xl mx-auto py-4">
    <div class="flex-auto block py-8 pt-6 px-9">
        <div class="overflow-x-auto">
            <div class="bg-white border border-4 rounded-lg shadow relative m-10">

                <div class="flex items-start justify-between p-5 border-b rounded-t">
                    <h3 class="text-xl font-semibold">
                        Create new meeting room
                    </h3>
                </div>

                <div class="p-6 space-y-6">
                    <form method="post" action="{{route('meeting-room.store')}}" enctype="multipart/form-data">
                        @csrf
                        <div class="grid grid-cols-6 gap-6">
                            <div class="col-span-6">
                                <label for="name" class="text-sm font-medium text-gray-900 block mb-2">
                                    Label
                                </label>
                                <input type="text" name="name" id="name"
                                    class="shadow-sm bg-gray-50 border border-gray-300 text-gray-900 sm:text-sm rounded-lg focus:ring-cyan-600 focus:border-cyan-600 block w-full p-2.5"
                                    required="">
                            </div>
                            <div class="col-span-6 sm:col-span-3">
                                <label for="address" class="text-sm font-medium text-gray-900 block mb-2">
                                    Address 1
                                </label>
                                <input type="text" name="address" id="address"
                                    class="shadow-sm bg-gray-50 border border-gray-300 text-gray-900 sm:text-sm rounded-lg focus:ring-cyan-600 focus:border-cyan-600 block w-full p-2.5"
                                    required="">
                            </div>
                            <div class="col-span-6 sm:col-span-3">
                                <label for="address2" class="text-sm font-medium text-gray-900 block mb-2">
                                    Address 2
                                </label>
                                <input type="text" name="address2" id="address2"
                                    class="shadow-sm bg-gray-50 border border-gray-300 text-gray-900 sm:text-sm rounded-lg focus:ring-cyan-600 focus:border-cyan-600 block w-full p-2.5">
                            </div>
                            <div class="col-span-6 sm:col-span-3">
                                <label for="contact1" class="text-sm font-medium text-gray-900 block mb-2">
                                    Contact 1
                                </label>
                                <input type="text" name="contact1" id="contact1"
                                    class="shadow-sm bg-gray-50 border border-gray-300 text-gray-900 sm:text-sm rounded-lg focus:ring-cyan-600 focus:border-cyan-600 block w-full p-2.5"
                                    placeholder="xx xx xx xx" required="">
                            </div>
                            <div class="col-span-6 sm:col-span-3">
                                <label for="contact2" class="text-sm font-medium text-gray-900 block mb-2">
                                    Contact 2
                                </label>
                                <input type="text" name="contact2" id="contact2"
                                    class="shadow-sm bg-gray-50 border border-gray-300 text-gray-900 sm:text-sm rounded-lg focus:ring-cyan-600 focus:border-cyan-600 block w-full p-2.5"
                                    placeholder="xx xx xx xx">
                            </div>
                            <div class="col-span-6 sm:col-span-2">
                                <label for="capacity" class="text-sm font-medium text-gray-900 block mb-2">
                                    Capacity
                                </label>
                                <input type="number" name="capacity" id="capacity"
                                    class="shadow-sm bg-gray-50 border border-gray-300 text-gray-900 sm:text-sm rounded-lg focus:ring-cyan-600 focus:border-cyan-600 block w-full p-2.5"
                                    placeholder="0" required="" min="1">
                            </div>
                            <div class="col-span-6 sm:col-span-2">
                                <label for="rental_cost" class="text-sm font-medium text-gray-900 block mb-2">
                                    Price
                                </label>
                                <input type="number" name="rental_cost" id="rental_cost"
                                    class="shadow-sm bg-gray-50 border border-gray-300 text-gray-900 sm:text-sm rounded-lg focus:ring-cyan-600 focus:border-cyan-600 block w-full p-2.5"
                                    placeholder="0" required="" min="1">
                            </div>
                            <div class="col-span-6 sm:col-span-2">
                                <label for="rental_during" class="text-sm font-medium text-gray-900 block mb-2">
                                    During
                                </label>
                                <input type="number" name="rental_during" id="rental_during"
                                    class="shadow-sm bg-gray-50 border border-gray-300 text-gray-900 sm:text-sm rounded-lg focus:ring-cyan-600 focus:border-cyan-600 block w-full p-2.5"
                                    placeholder="0" required="" min="1">
                            </div>
                            <div class="col-span-full">
                                <label for="description" class="text-sm font-medium text-gray-900 block mb-2">
                                    Description
                                </label>
                                <textarea id="description" name="description" rows="6"
                                    class="bg-gray-50 border border-gray-300 text-gray-900 sm:text-sm rounded-lg focus:ring-cyan-600 focus:border-cyan-600 block w-full p-4"
                                    placeholder="Details"></textarea>
                            </div>
                            <div class="col-span-full">
                                <label for="other_details" class="text-sm font-medium text-gray-900 block mb-2">
                                    Other deails
                                </label>
                                <textarea id="other_details" name="other_details" rows="6"
                                    class="bg-gray-50 border border-gray-300 text-gray-900 sm:text-sm rounded-lg focus:ring-cyan-600 focus:border-cyan-600 block w-full p-4"
                                    placeholder="Other Details"></textarea>
                            </div>
                            <div class="col-span-6">
                                <label for="price" class="text-sm font-medium text-gray-900 block mb-2">
                                    Images
                                </label>
                                <input type="file" name="photos[]" id="price" multiple="multiple" max="4"
                                    class="shadow-sm bg-gray-50 border border-gray-300 text-gray-900 sm:text-sm rounded-lg focus:ring-cyan-600 focus:border-cyan-600 block w-full p-2.5"
                                    required="">
                            </div>
                        </div>
                        <div class="p-6 border-t border-gray-200 rounded-b">
                            <button
                                class="text-white bg-cyan-600 hover:bg-cyan-700 focus:ring-4 focus:ring-cyan-200 font-medium rounded-lg text-sm px-5 py-2.5 text-center"
                                type="submit">Save Room</button>
                        </div>
                    </form>
                </div>


            </div>
        </div>
    </div>
</main>
@endsection
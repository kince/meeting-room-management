<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('reservations', function (Blueprint $table) {
            $table->bigInteger('id')->autoIncrement();
            $table->enum('status', ['pending', 'confirmed', 'finished', 'canceled', 'in_progress'])->default('pending');
            $table->dateTime('start_date');
            $table->dateTime('end_date');
            $table->timestamps();

            $table->softDeletes();

            $table->foreignIdFor(\App\Models\User::class, 'user_id')
                ->constrained()
                ->onUpdate('cascade')
                ->onDelete('cascade');

            $table->foreignIdFor(\App\Models\MeetingRoom::class, 'meeting_room_id')
                ->constrained()
                ->onUpdate('cascade')
                ->onDelete('cascade');

            $table->primary(['id', 'user_id', 'meeting_room_id']);
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('reservations');
    }
};

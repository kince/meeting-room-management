<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('meeting_rooms', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->string('address');
            $table->string('address2')->nullable();
            $table->string('contact1')->nullable();
            $table->string('contact2')->nullable();
            $table->integer('capacity');
            $table->double('rental_cost');
            $table->integer('rental_during')->comment("temps de mesure pour le cout de location");
            $table->text('description')->nullable();
            $table->text('other_details')->nullable();
            $table->timestamps();

            $table->softDeletes();

            $table->foreignIdFor(\App\Models\User::class, 'owner_id')
                ->constrained('users', 'id')
                ->onUpdate('cascade')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('meeting_rooms');
    }
};

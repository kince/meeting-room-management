<?php

namespace Database\Seeders;

use App\Models\User;
// use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;
use Spatie\Permission\Models\Role;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     */
    public function run(): void
    {
        Role::create(['name' => 'admin']);
        Role::create(['name' => 'announcer']);
        Role::create(['name' => 'user']);

        $user = User::factory()->create([
            'name' => 'Admin User',
            'email' => 'admin@mrm.com',
            'phone' => '21222226',
            'address' => 'Lomé, Baguida',
            'password' => Hash::make("password"),
        ]);

        $client = User::factory()->create([
            'name' => 'Client User',
            'email' => 'client@mrm.com',
            'phone' => '22232326',
            'address' => 'Lomé, Kpgan',
            'password' => Hash::make("password"),
        ]);

        $announcer = User::factory()->create([
            'name' => 'Announcer User',
            'email' => 'announcer@mrm.com',
            'phone' => '22232626',
            'address' => 'Lomé, Kpogan',
            'password' => Hash::make("password"),
        ]);

        $user->assignRole('admin');
        $client->assignRole('user');
        $announcer->assignRole('announcer');
    }
}

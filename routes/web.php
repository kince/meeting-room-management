<?php

use App\Http\Controllers\MainController;
use App\Http\Controllers\MeetingRoomController;
use App\Http\Controllers\ReservationController;
use App\Http\Middleware\IsAdmin;
use Illuminate\Support\Facades\Route;

Route::get('/', [MainController::class, 'home'])->name('client.home');

Route::get('/client/meeting-rooms/{id}', [MainController::class, 'show_mr'])->name('client.show_mr');

Route::middleware('auth')->group(function () {
    Route::post('/meeting-rooms/book', [ReservationController::class, 'book'])->name('client.book');

    Route::get('/client/reservations', [ReservationController::class, 'show_user_reservation'])->name('client.reservations');

    Route::get('/client/reservations/{id}/cancel', [ReservationController::class, 'cancel_book'])->name('client.reservations.cancel');
});

Route::middleware(['auth', IsAdmin::class])->group(function () {
    // Route::get('/dashboard', function () {
    //     return view('dashboard');
    // })->name('dashboard');

    // Route::get('/meeting-rooms', [MeetingRoomController::class, 'index'])->name('meeting-room.index');
    Route::get('/dashboard', [MeetingRoomController::class, 'index'])->name('meeting-room.index');

    Route::get('/meeting-rooms/create', [MeetingRoomController::class, 'create'])->name('meeting-room.create');

    Route::get('/meeting-rooms/{id}', [MeetingRoomController::class, 'show'])->name('meeting-room.show');

    Route::post('/meeting-room', [MeetingRoomController::class, 'store'])->name('meeting-room.store');

    Route::put('/meeting-rooms/{id}', [MeetingRoomController::class, 'update'])->name('meeting-room.update');

    Route::delete('/meeting-rooms/{id}', [MeetingRoomController::class, 'delete'])->name('meeting-room.delete');

    Route::get('/reservations', [ReservationController::class, 'all_reservation'])->name('reservation.index');
});

require __DIR__ . '/auth.php';

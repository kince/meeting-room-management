<p align="center">
<a href="https://github.com/laravel/framework/actions"><img src="https://github.com/laravel/framework/workflows/tests/badge.svg" alt="Build Status"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://img.shields.io/packagist/dt/laravel/framework" alt="Total Downloads"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://img.shields.io/packagist/v/laravel/framework" alt="Latest Stable Version"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://img.shields.io/packagist/l/laravel/framework" alt="License"></a>
</p>

## How to install project

1. Run git clone https://gitlab.com/kince/meeting-room-management.git
2. Run composer install
3. Run cp .env.example .env
4. Run php artisan key:generate
5. Run npm install
6. Run php artisan storage:link
7. Run php artisan migrate --seed
8. Run npm run dev
9. Run php artisan serve
10. Go to link localhost:8000
11. Admin Crédentials: {Email address=admin@mrm.com; Mot de passe=password}

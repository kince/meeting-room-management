<?php

namespace App\Http\Controllers;

use App\Models\MeetingRoom;
use Illuminate\Http\Request;

class MainController extends Controller
{
    public function home()
    {
        $meeting_rooms = MeetingRoom::with(['medias', 'owner'])->get();

        return view('client.index', compact('meeting_rooms'));
    }

    public function show_mr(string $id)
    {
        $meeting_room = MeetingRoom::with(['medias', 'owner'])->findOrFail($id);

        return view('client.mr-detail', compact('meeting_room'));
    }
}

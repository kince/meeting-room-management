<?php

namespace App\Http\Controllers;

use App\Models\Media;
use App\Models\MeetingRoom;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use RealRashid\SweetAlert\Facades\Alert;

class MeetingRoomController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $meeting_rooms = MeetingRoom::with(['medias', 'owner'])->get();

        return view('dashboard.list-of-mr', compact('meeting_rooms'));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        return view('dashboard.create-mr');
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $rules =  [
            'name'          => 'required|string',
            'address'       => 'required|string',
            'address2'      => 'required|string',
            'contact1'      => 'required|string',
            'contact2'      => 'nullable|string',
            'capacity'      => 'required|integer',
            'rental_cost'   => 'required|decimal:2',
            'rental_during' => 'required|integer',
            'description'   => 'required|string',
            'other_details' => 'nullable|string',
            'photos.*'      => 'image|mimes:jpeg,png,jpg|max:4096',
        ];

        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            toast('Fields validation error.', 'error');
        }
        DB::beginTransaction();
        try {
            $meeting_room = MeetingRoom::create([
                'name'          => $request->name,
                'address'       => $request->address,
                'address2'      => $request->address2,
                'contact1'      => $request->contact1,
                'contact2'      => $request->contact2,
                'capacity'      => $request->capacity,
                'rental_cost'   => $request->rental_cost,
                'rental_during' => $request->rental_during,
                'description'   => $request->description,
                'other_details' => $request->other_details,
                'owner_id'      => Auth::user()->id,
            ]);

            if ($request->hasFile('photos')) {
                foreach ($request->file('photos') as $key => $image) {
                    $imgName = time() . '-' . $image->getClientOriginalName();
                    $image->storeAs('public/mr-images', $imgName);

                    Media::create([
                        'file_path' => $imgName,
                        'meeting_room_id' => $meeting_room->id,
                    ]);
                }
            }

            DB::commit();

            toast('Meeting room added successully.', 'success');
            return redirect()->back();
        } catch (\Exception $ex) {
            DB::rollBack();
            toast($ex->getMessage(), 'error');
            return redirect()->back();
        }
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        $meeting_room = MeetingRoom::with(['medias', 'owner', 'reservations'])->findOrFail($id);
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        $meeting_room = MeetingRoom::with(['medias', 'owner', 'reservations'])->findOrFail($id);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        $meeting_room = MeetingRoom::with(['medias', 'owner', 'reservations'])->findOrFail($id);
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        $meeting_room = MeetingRoom::with(['owner'])->where('id', $id)->get();

        if (!$meeting_room) {
            toast("Target meeting room not found.", 'error');
        }

        if (($meeting_room->owner->id != Auth::user()->id) && (!$meeting_room->owner->hasRole('admin'))) {
            toast("You're not allowed.", 'error');
        }

        $meeting_room->delete();
        toast("Meeting delete successfully.", 'success');
    }
}

<?php

namespace App\Http\Controllers;

use App\Enums\ReservationStatusEnum;
use App\Models\MeetingRoom;
use App\Models\Reservation;
use Illuminate\Contracts\View\View;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class ReservationController extends Controller
{
    public function all_reservation()
    {
        $reservations = Reservation::with(['meetingRoom', 'user'])->get();

        return view('dashboard.list-of-reservations', compact('reservations'));
    }

    public function show_user_reservation()
    {
        $reservations = Reservation::with(['meetingRoom', 'user'])->where('user_id', Auth::id())->get();

        return view('client.list-of-reservations', compact('reservations'));
    }

    public function book(Request $request)
    {
        // dd($request);
        $validator = Validator::make($request->all(), [
            'meeting_room_id' => 'required|exists:meeting_rooms,id',
            'start_date' => 'required|date',
            'end_date' => 'required|date|after:start_date',
        ]);

        if ($validator->fails()) {
            toast("Fields validation fail.", 'error');
            return redirect()->back()->withErrors($validator)->withInput();
        }

        $meeting_room_id = $request->input('meeting_room_id');
        $start_date = $request->input('start_date');
        $end_date = $request->input('end_date');

        try {
            $meeting_room = MeetingRoom::find($meeting_room_id);
            $user = Auth::user()->id;

            // check if meeting room is available
            if (!$meeting_room->is_available()) {
                toast("The meeting room is already booked.", 'error');
                return redirect()->back()->with("error", "already booked");
            }

            $reservation = Reservation::create([
                'status'            => ReservationStatusEnum::Pending,
                'start_date'        => $start_date,
                'end_date'          => $end_date,
                'user_id'           => $user,
                'meeting_room_id'   => $meeting_room->id,
            ]);

            toast("Successfully booked meeting room.", 'success');
            return redirect()->back();
        } catch (\Exception $ex) {
            toast("Meeting room reservation failure.", 'error');
            return redirect()->back()->with('error', $ex->getMessage());
        }
    }

    public function cancel_book(string $id)
    {
        try {
            $reservation = Reservation::find($id);
            $reservation->status = ReservationStatusEnum::Canceled;
            $reservation->save();

            toast("Successfully canceled meeting room reservation.", 'success');
            return redirect()->back();
        } catch (\Exception $ex) {
            toast("Meeting room reservation cancellation failure.", 'error');
            return redirect()->back()->with('error', $ex->getMessage());
        }
    }
}

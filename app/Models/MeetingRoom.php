<?php

namespace App\Models;

use App\Enums\ReservationStatusEnum;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class MeetingRoom extends Model
{
    use HasFactory, SoftDeletes;

    protected $table = "meeting_rooms";

    protected $fillable = [
        'name',
        'address',
        'address2',
        'contact1',
        'contact2',
        'capacity',
        'rental_cost',
        'rental_during',
        'description',
        'other_details',
        'owner_id',
    ];

    public function owner()
    {
        return $this->belongsTo(User::class, 'owner_id');
    }

    public function reservations()
    {
        return $this->hasMany(Reservation::class, 'meeting_room_id', 'id');
    }

    public function medias()
    {
        return $this->hasMany(Media::class, 'meeting_room_id', 'id');
    }

    public function is_available()
    {
        $latestReservation = $this->reservations()->orderBy('created_at', 'desc')->first();

        if (!$latestReservation) {
            return true;
        }

        return $latestReservation->status == "canceled" || $latestReservation->status == "finished";
    }
}

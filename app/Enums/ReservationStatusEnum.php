<?php

namespace App\Enums;

enum ReservationStatusEnum: string
{
    case Pending = "pending";
    case Confirmed = "confirmed";
    case Finished = "finished";
    case Canceled = "canceled";
    case InProgress = "in_progress";
}
